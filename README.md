# pandoc-latex
Creating a docker image with pandoc and pfdlatex

## Tools
* pandoc
* pdf latex

## Image Name
The image is bundled as `pandoc-latex`

## Usage
The image can used for automatically creating pdf/docx files from the md files with pandoc and pdflatex through CI/CD pipelines

## Info
Use the .dockeringore file as whitelist to make the build simply faster to not copy the complete folder during `docker build`

## Image
Repo available under https://hub.docker.com/r/lweiss89/pandoc-latex
```
docker pull lweiss89/pandoc-latex
```